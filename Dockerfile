FROM debian:bookworm-slim
ARG HAUTEUR
RUN apt-get -y update && apt-get install -y --no-install-recommends gforth && apt clean && apt autoremove
COPY pyramide.fs /
COPY run.sh /
ENV HAUTEUR=$HAUTEUR
ENTRYPOINT ["/bin/sh","-c","/run.sh $HAUTEUR"]
