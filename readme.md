# dessine une pyramide constituée  du caractère `*`  dans le terminal

lancement manuel : `gforth pyramide.fs 10` va donner

une pyramide comme ci dessous : 

```
         /*\
        /***\
       /*****\
      /*******\
     /*********\
    /***********\
   /*************\
  /***************\
 /*****************\

```

## conseil visuel 

Pour un bon résultat visuel il est conseillé d'utiliser un nombre de lignes allant de 10 à 50

Il est rare d'avoir un écran/terminal plus haut/large permettant un affichage propre.

## code original 

![capture](android-gforth.jpg)

# REF 

![logo](logo.png)
 https://www.gnu.org/software/gforth/

# Automatiser le processus dans un Dockerfile

```
FROM debian:latest
ARG HAUTEUR
RUN apt-get -y update && apt-get install -y gforth
COPY pyramide.fs /
COPY run.sh /
ENV HAUTEUR=$HAUTEUR
ENTRYPOINT ["/bin/sh","-c","/run.sh $HAUTEUR"]
```

Comment construire et lancer notre container : 

```
Sending build context to Docker daemon    490kB
Step 1/7 : FROM debian:latest
 ---> c69ac6bed067
Step 2/7 : ARG HAUTEUR
 ---> Using cache
 ---> 8ae529cf3c77
Step 3/7 : RUN apt-get -y update && apt-get install -y gforth
 ---> Using cache
 ---> f066305658d7
Step 4/7 : COPY pyramide.fs /
 ---> Using cache
 ---> c8a02ccb16ec
Step 5/7 : COPY run.sh /
 ---> Using cache
 ---> 287d27078aa8
Step 6/7 : ENV HAUTEUR=$HAUTEUR
 ---> Using cache
 ---> 209089c93c59
Step 7/7 : ENTRYPOINT ["/bin/sh","-c","/run.sh $HAUTEUR"]
 ---> Using cache
 ---> b33d23e3c71b
Successfully built b33d23e3c71b
Successfully tagged francoispussault/pyramide:latest
``` 

On peut lancer un test : `./run.sh 10` or `./run.sh`

```
         /*\
        /***\
       /*****\
      /*******\
     /*********\
    /***********\
   /*************\
  /***************\
 /*****************\
```

